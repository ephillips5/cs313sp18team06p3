package edu.luc.etl.cs313.android.shapes.android;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import edu.luc.etl.cs313.android.shapes.model.*;
import java.util.List;

/**
 * A Visitor for drawing a shape to an Android canvas.
 */
//OK

public class Draw implements Visitor<Void> {

	// TODO entirely your job (except onCircle)
    //I think Fill and Outline need to call set Style, while Stroke only
    //calls setColor()

	private final Canvas canvas;

	private final Paint paint;

	public Draw(final Canvas canvas, final Paint paint) {
		this.canvas = canvas; // FIXME
		this.paint = paint; // FIXME
		paint.setStyle(Style.STROKE);
	}

	@Override
	public Void onCircle(final Circle c) {
		canvas.drawCircle(0, 0, c.getRadius(), paint);
		return null;
	}

	@Override
	public Void onStroke(final Stroke c) {
        Shape strokeShape = c.getShape();
        int colorStroke = c.getColor();
		paint.setColor(colorStroke);
		strokeShape.accept(this);
		paint.setColor(colorStroke);
		return null;
	}

	@Override
	public Void onFill(final Fill f) {
        Shape fillShape = f.getShape();
        paint.setStyle(Style.FILL_AND_STROKE);
        fillShape.accept(this);
        paint.setStyle(Style.FILL_AND_STROKE);
		return null;
	}

	@Override
	public Void onGroup(final Group g) {
		List<? extends Shape> groupShapes = g.getShapes();
		for (Shape shape: groupShapes)
		{
			shape.accept(this);
		}
		return null;
	}

	@Override
	public Void onLocation(final Location l) {
		Shape locShape = l.getShape();
		canvas.translate(l.getX(),l.getY());
		//Will call the certain shape's draw method
		locShape.accept(this);
		canvas.translate(-l.getX(),-l.getY());
		return null;
	}

	@Override
	public Void onRectangle(final Rectangle r) {
		canvas.drawRect(0,0,r.getWidth(),r.getHeight(),paint);
		return null;
	}

	@Override
	public Void onOutline(Outline o) {
		Shape outlineShape = o.getShape();
		paint.setStyle(Style.STROKE);
		outlineShape.accept(this);
		paint.setStyle(Style.STROKE);
		return null;
	}

	@Override
	public Void onPolygon(final Polygon s) {
		//Can get List of points using Polygon's getPoints() method
		//Then need to convert list to an array and get it to hold floats
		List<? extends Point> polyPoints = s.getPoints();
		final float[] pts = new float[polyPoints.size()];
		for (int i = 0; i < pts.length;i+=2)
		{
			pts[i] = (float)polyPoints.get(i).getX();
			pts[i+1] = (float)polyPoints.get(i).getY();
		}
		canvas.drawLines(pts, paint);
		return null;
	}
}
