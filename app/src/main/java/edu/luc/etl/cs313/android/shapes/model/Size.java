package edu.luc.etl.cs313.android.shapes.model;
import java.util.List;

/**
 * A visitor to compute the number of basic shapes in a (possibly complex)
 * shape.
 */
public class Size implements Visitor<Integer> {

	// TODO entirely your job

	@Override
	public Integer onPolygon(final Polygon p) {
		return 1;
	}

	@Override
	public Integer onCircle(final Circle c) {
		return 1;
	}

	@Override
	public Integer onGroup(final Group g) {
		final List<? extends Shape> shapes = g.getShapes();
		Integer counter = 0;
		for (Shape s: shapes)
		{
			counter += s.accept(this);		//fix
		}
		return counter;
	}

	@Override
	public Integer onRectangle(final Rectangle q) {
		return 1;
	}

	@Override
	public Integer onOutline(final Outline o) {
		final Shape child = o.getShape();
		Integer count = child.accept(this);
		return count;
	}

	@Override
	public Integer onFill(final Fill c)
	{
		final Shape child = c.getShape();
		Integer count = child.accept(this);
		return count;
	}

	@Override
	public Integer onLocation(final Location l) {
		final Shape child = l.getShape();
		Integer count = child.accept(this);
		return count;
	}

	@Override
	public Integer onStroke(final Stroke c) {
		final Shape child = c.getShape();
		Integer count = child.accept(this);
		return count;
	}
}

//Need to implement all of the methods-- look at unit tests