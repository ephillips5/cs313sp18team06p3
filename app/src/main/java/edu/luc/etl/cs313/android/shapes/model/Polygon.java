package edu.luc.etl.cs313.android.shapes.model;

import java.util.List;

/**
 * A special case of a group consisting only of Points.
 *
 */
public class Polygon extends Group {

	public Polygon(final Point... points) {
		super(points);
	}

	@SuppressWarnings("unchecked")
	public List<? extends Point> getPoints() {
		return (List<? extends Point>) getShapes();
	}

	@Override
	public <Result> Result accept(final Visitor<Result> v) {
		return v.onPolygon(this);
	}
}

//Points are used in the Polygon shape
//Generates a list of points

//Three visitors
//Draw Visitor-- just draws the shape
//Size Visitor-- returns an integer that's the size of the shape
//Bounding box-- visitor that returns the smallest rectangle that encompasses the set of shapes that are drawn