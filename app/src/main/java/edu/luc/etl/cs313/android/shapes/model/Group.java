package edu.luc.etl.cs313.android.shapes.model;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * A composite for grouping edu.luc.etl.cs313.android.shapes.model.
 */
public class Group implements Shape {

	protected final List<? extends Shape> shapes;

	public Group(final Shape... shapes) {
		this.shapes = Arrays.asList(shapes);
	}

	public List<? extends Shape> getShapes() {
		return Collections.unmodifiableList(shapes);
	}

	@Override
	public <Result> Result accept(final Visitor<Result> v) {
		return v.onGroup(this);
	}

}


// A Group is a composite
// Has a list of 0 or more Shapes
// Constructor takes 0 or more Shape parameters
//Pass in number of parameters to the Group constructor-- converts parameters to an array of Shapes
//Creates array from number of parameters-- array of Shape references

//Accept method returns v.onGroup(this)-- visitor can use methods
//v.onGroup must take a single Group parameter and then the visitor can use the parameter to access Group's methods