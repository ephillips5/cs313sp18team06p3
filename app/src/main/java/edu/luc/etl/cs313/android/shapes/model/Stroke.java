package edu.luc.etl.cs313.android.shapes.model;

import android.graphics.Color;

/**
 * A decorator for specifying the stroke (foreground) color for drawing the
 * shape.
 */
public class Stroke implements Shape {

	// TODO entirely your job
	//Instantiate a Color object and return in getColor()

	protected final Shape shape;
	protected int colorInt;

	public Stroke(final int color, final Shape shape) {
		this.shape = shape;
		this.colorInt = color;
	}

	public int getColor()
	{
		return colorInt;
	}

	public Shape getShape() {
		return shape;
	}

	@Override
	public <Result> Result accept(Visitor<Result> v) {
		return v.onStroke(this);
	}
}
