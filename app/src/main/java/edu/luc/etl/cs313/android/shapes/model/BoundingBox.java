package edu.luc.etl.cs313.android.shapes.model;

import java.util.Collections;
import java.util.List;

/**
 * A shape visitor for calculating the bounding box, that is, the smallest
 * rectangle containing the shape. The resulting bounding box is returned as a
 * rectangle at a specific location.
 */
public class BoundingBox implements Visitor<Location> {

	// TODO entirely your job (except onCircle)

	//For onGroup(), onStroke(), onOutline(), and onFill
		//Get the shapes in the objects
		//Call their accept methods to get the bounding boxes of the various shapes

	//Do all of the variables in the methods need to be final?

	@Override
	public Location onCircle(final Circle c) {
		final int radius = c.getRadius();
		return new Location(-radius, -radius, new Rectangle(2 * radius, 2 * radius));
	}

	@Override
	public Location onFill(final Fill f) {
		final Shape fillShape = f.getShape();
		final Location locFillShape = fillShape.accept(this);
		return locFillShape;
	}

	@Override
	public Location onGroup(final Group g) {
		//X and Y coordinates of the Bounding Box are
		// the minimum of the x and y coordinates of the group
		//Width and height of the rectangle are based on the max x and y coordinates
		//of the group minus the upper left x and y coordinates-- min coordinates
		int minX = Integer.MAX_VALUE;
		int minY = Integer.MAX_VALUE;
		int maxX = Integer.MIN_VALUE;
		int maxY = Integer.MIN_VALUE;

		final List<? extends Shape> shapes = g.getShapes();
		for (Shape shape: shapes)
		{
			Location locBounding = shape.accept(this);
			int locX = locBounding.getX();
			int locY = locBounding.getY();

			Rectangle rectBounding = (Rectangle)locBounding.getShape();
			int xAndWidth = locX + rectBounding.getWidth();
			int yAndHeight = locY + rectBounding.getHeight();

			if (locX< minX)
			{
				minX = locX;
			}
			if (locY< minY)
			{
				minY = locY;
			}
			if (xAndWidth > maxX)
			{
				maxX = xAndWidth;
			}
			if (yAndHeight> maxY)
			{
				maxY = yAndHeight;
			}
		}
		return new Location(minX,minY,new Rectangle((maxX-minX),(maxY-minY)));
	}

	@Override
	public Location onLocation(final Location l) {
		final int locX = l.getX();
		final int locY = l.getY();

		final Shape locShape = l.getShape();
		final Location locBounding = locShape.accept(this);
		int locBX = locBounding.getX();
		int locBY = locBounding.getY();
		return new Location((locX+locBX),(locY+locBY),locBounding.getShape());
	}

	@Override
	public Location onRectangle(final Rectangle r) {
		return new Location(0,0,r);
	}

	@Override
	public Location onStroke(final Stroke c) {
		final Shape strokeShape = c.getShape();
		final Location locStrokeShape = strokeShape.accept(this);
		return locStrokeShape;
	}

	@Override
	public Location onOutline(final Outline o) {
		final Shape outlineShape = o.getShape();
		final Location locOutlineShape = outlineShape.accept(this);
		return locOutlineShape;
	}

	@Override
	public Location onPolygon(final Polygon s) {
		return onGroup(s);
	}
}

//Bounding box for each shape and a group of shapes
//Traverses each shape and returns a bounding box that has a location and size around the shape
//Bounding Box for the group-- uses upper left corners of the shapes in the group
//to find the maximum x and y for all of the shapes that have been traversed
